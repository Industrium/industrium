module tristate #(N = 16) (
	input wire Clk, OE,
	input [N-1:0] In,
	output logic [N-1:0] Out,
	inout wire [N-1:0] SRAM_DQ
);

logic [N-1:0] a, b;

assign SRAM_DQ = (OE) ? a : {N{1'bZ}}; //if writing srambf use dq
assign Out = b;

//assign SRAM_DQ = OE ? In : {N{1'bZ}}; //if not reading write
//assign Out = SRAM_DQ;

always_ff @(posedge Clk)
begin
	b <= SRAM_DQ;
	a <= In;
end

endmodule
