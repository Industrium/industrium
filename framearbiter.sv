module framearbiter (input writesig, switch,
							input [19:0] drawaddr, copyaddr,
							input [18:0] buffer2addr,
							output logic [18:0] fbaddr,
							output logic [19:0] SRAM_ADDR,
							output logic write
							);

/**************************second frame buffer address choose*******************/							
assign fbaddr = (switch == 1'b1) ? buffer2addr : 19'bZZZZZZZZZZZZZZZZZZZ; 	

/***************************second frame buffer writeen**************************/
assign write = (switch == 1'b1) ? writesig : 0;

/***************************SRAM_ADDR******************************************/										
assign SRAM_ADDR = (switch == 1'b0) ? copyaddr : drawaddr;	

endmodule