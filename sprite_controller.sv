module  sprite_controller ( input Reset, frame_clk, enemydeath, transparent, //only update at vertical sync
									input [15:0] Keycode, //from keyboard
									input [6:0] motion,
									output logic [10:0]  spritex, spritey,
									output logic [10:0] soldierx, soldiery,
									output logic [10:0] soldierxmotion, soldierymotion,
									output logic [10:0] soldierxpos, soldierypos,	
									output logic [10:0] Sprite_XPos, Sprite_YPos,
									output logic hop 
									);		
									
 	logic [10:0]  Sprite_XMotion, Sprite_YMotion;
	
	 logic [10:0] soldierxstart, soldierystart;

	 logic [10:0] Spriteloc; //temp location
	 logic [10:0] Spritejmp; //temp jump location
	 
	logic [10:0]	 Sprite_Xstart;  // Start position on the X axis
	logic [10:0]	 Sprite_Ystart;  // Start position on the Y axis
	
	always_comb
	begin

			Sprite_Xstart = 10;  // Start position on the X axis
			Sprite_Ystart = 300;  // Start position on the Y axis				
			soldierxstart = 500;
			soldierystart = 324;
	end
	
	
    parameter [10:0] Sprite_XMin=10;       // Leftmost point on the X axis
    parameter [10:0] Sprite_XMax=640;     // Rightmost point on the X axis
    parameter [10:0] Sprite_YMin=10;       // Topmost point on the Y axis
    parameter [10:0] Sprite_YMax=479;     // Bottommost point on the Y axis
	     parameter [10:0] Move_YMin=250;     // Bottommost point on the Y axis
//		  parameter [10:0] Move_YMax=356;     // Bottommost point on the Y axis
    parameter [10:0] Sprite_XStep=4;      // Step size on the X axis
    parameter [10:0] Sprite_YStep=4;      // Step size on the Y axis
	 parameter [10:0] soldierxstep = 1;
	 parameter [10:0] soldierystep = 1;
   
	 logic count;
    parameter [9:0] Jump = 150;	  //Jump height
	
    always_ff @ (posedge Reset or posedge frame_clk )
    begin: Move_Ball
        if (Reset)  // Asynchronous Reset
        begin 
         Sprite_YMotion <= 11'd0; //Sprite_YStep;
			Sprite_XMotion <= 11'd0; //Sprite_XStep;
			Sprite_YPos <= Sprite_Ystart;
			Sprite_XPos <= Sprite_Xstart;
         soldierymotion <= 11'd0; //Sprite_YStep;
			soldierxmotion <= 11'd0; //Sprite_XStep;
			soldierypos <= soldierystart;
			soldierxpos <= soldierxstart;			
			Spriteloc <= 11'd0;
			hop <= 1'b0;
			count <= 1'b0;
        end
			else
			begin
				Sprite_XMotion <= Sprite_XMotion;// keep both motions
				Sprite_YMotion <= Sprite_YMotion;
				soldierxmotion <= soldierxmotion;
				soldierymotion <= soldierymotion;
		  
				case(Keycode) //only take in keycode when we're not doing anything. 

/***************************************JUMP*********************************************/				
					16'h0044: //jump
						begin
							if(count == 1'b0)
							begin
								Spriteloc <= Sprite_YPos;
								Spritejmp <= Sprite_YPos - Jump;
								count <= 1'b1;
								hop <= 1'b1;
							end
							if((Sprite_YPos) > Spritejmp)
							begin
								Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);
								hop <= 1'b1;
								count <= 1'b1;
							end
							else 
							begin
								if(Sprite_YPos == Spriteloc)
								begin
									Sprite_YMotion <= 11'd0;
									hop <= 1'b0;
									count <= 1'b0;
								end
								else
								begin
									Sprite_YMotion <= Sprite_YStep;		
									count <= 1'b1;
									hop <= 1'b1;						
								end
							end
						end
						
/***************************************LEFT VARIATIONS********************************/				  
				  'd80: //Left
					begin
						Sprite_YMotion <= 11'd0;
						
						if ((Sprite_XPos) <= Sprite_XMin)
							Sprite_XMotion <= 11'd0;
						else
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1); //left
					end
					
					 'h5250: //left and up
					 begin	
						if(((Sprite_XPos) <= Sprite_XMin) && ((Sprite_YPos) <= Move_YMin))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ( (Sprite_YPos) <= Move_YMin )
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							end
						else if ((Sprite_XPos) <= Sprite_XMin)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);
							end
						else
							begin
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);	 
							end
					 end
					 
					 'h5052: //left and up 
					 begin
						if(((Sprite_XPos) <= Sprite_XMin) && ((Sprite_YPos) <= Move_YMin))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ( (Sprite_YPos) <= Move_YMin )
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							end
						else if ((Sprite_XPos) <= Sprite_XMin)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);
							end
						else
							begin
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);	 
							end
					 end
					 
					 'h5150: //left and down
					 begin
						if(((Sprite_XPos) <= Sprite_XMin) && ((Sprite_YPos + 124) >= Sprite_YMax))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ( (Sprite_YPos + 124) >= Sprite_YMax )
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							end
						else if ((Sprite_XPos) <= Sprite_XMin)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= Sprite_YStep;
							end
						else
							begin
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							Sprite_YMotion <= Sprite_YStep;	 
							end
					 end
					 
					 'h5051: //left and down	
					 begin
						if(((Sprite_XPos) <= Sprite_XMin) && ((Sprite_YPos + 124) >= Sprite_YMax))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ( (Sprite_YPos + 124) >= Sprite_YMax )
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							end
						else if ((Sprite_XPos) <= Sprite_XMin)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= Sprite_YStep;
							end
						else
							begin
							Sprite_XMotion <= (~(Sprite_XStep) + 1'b1);
							Sprite_YMotion <= Sprite_YStep;	 
							end			 
					 end					 
					
/***************************************RIGHT VARIATIONS*****************************/					
				  'd79://Right
					begin
						Sprite_YMotion <= 11'd0;
						
						if ((Sprite_XPos + 140) >= Sprite_XMax)
							Sprite_XMotion <= 11'd0;
						else
							Sprite_XMotion <= Sprite_XStep;
					end
					
					'h524f: //right and up
					begin
						if(((Sprite_XPos+140) >= Sprite_XMax) && ((Sprite_YPos) <= Move_YMin))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ( (Sprite_YPos) <= Move_YMin )
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= Sprite_XStep;
							end
						else if ((Sprite_XPos+140) >= Sprite_XMax)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);
							end
						else
							begin
							Sprite_XMotion <= Sprite_XStep;
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);	 
							end					
					end
					
					'h4f52: //right and up
					begin
						if(((Sprite_XPos+140) >= Sprite_XMax) && ((Sprite_YPos) <= Move_YMin))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ( (Sprite_YPos) <= Move_YMin )
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= Sprite_XStep;
							end
						else if ((Sprite_XPos+140) >= Sprite_XMax)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);
							end
						else
							begin
							Sprite_XMotion <= Sprite_XStep;
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1);	 
							end	
					end	
					
					'h514f: //down and right
					begin
						if(((Sprite_XPos+140) >= Sprite_XMax) && ((Sprite_YPos+124) >= Sprite_YMax))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ((Sprite_YPos+124) >= Sprite_YMax)
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= Sprite_XStep;
							end
						else if ((Sprite_XPos+140) >= Sprite_XMax)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= Sprite_YStep;
							end
						else
							begin
							Sprite_XMotion <= Sprite_XStep;
							Sprite_YMotion <= Sprite_YStep;	 
							end						
					end			
					
					'h4f51: //down and right
					begin
						if(((Sprite_XPos+140) >= Sprite_XMax) && ((Sprite_YPos+124) >= Sprite_YMax))
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= 11'd0;
							end
						else if ((Sprite_YPos+124) >= Sprite_YMax)
							begin
							Sprite_YMotion <= 11'd0;
							Sprite_XMotion <= Sprite_XStep;
							end
						else if ((Sprite_XPos+140) >= Sprite_XMax)
							begin
							Sprite_XMotion <= 11'd0;	
							Sprite_YMotion <= Sprite_YStep;
							end
						else
							begin
							Sprite_XMotion <= Sprite_XStep;
							Sprite_YMotion <= Sprite_YStep;	 
							end		
					end	
	
/************************************UP AND DOWN*********************************/	
					'h0052: //up
					begin
						Sprite_XMotion <= 11'd0; //go in x motion
						
						if((Sprite_YPos) <= Move_YMin)
							Sprite_YMotion <= 11'd0;
						else
							Sprite_YMotion <= (~(Sprite_YStep) + 1'b1); //stop y motion
					end		
					
					'h0051: //down
					begin
						Sprite_XMotion <= 11'd0; //go in x motion
	
						if((Sprite_YPos+124) >= Sprite_YMax)
							Sprite_YMotion <= 11'd0;
						else
							Sprite_YMotion <= Sprite_YStep; //stop y motion
					end					
	
/***********************************DEFAULT*****************************************/	
				  default:
					begin
						Sprite_YMotion <= 11'd0; //Stop
						Sprite_XMotion <= 11'd0; //Moving
					end
					
				endcase

/*********************************Soldier AI**************************************/	
			
				if(enemydeath == 1'b1 || (enemydeath == 1'b0 && transparent == 1'b1))
					begin
						soldierxmotion <= 11'd0;
						soldierymotion <= 11'd0;
					end
				else if(motion == 6'd13 || //attack motions
					motion == 6'd14 || 
					motion == 6'd15 ||
					motion == 6'd16 ||
					motion == 6'd17 ||
					motion == 6'd28)
				begin
					soldierymotion <= 11'd0; 
					soldierxmotion <= 11'd0; 				
				end			
				else if(((soldierypos - 20) < Sprite_YPos) && (soldierxpos-16 < Sprite_XPos)) //top left
				begin
					soldierxmotion <= soldierxstep;					
					soldierymotion <= soldierystep;				
				end			
				else if(((soldierypos - 20) < Sprite_YPos) && (soldierxpos-16 > Sprite_XPos)) //top right
				begin
					soldierxmotion <= ~soldierxstep + 1'b1;					
					soldierymotion <= soldierystep;				
				end
				else if(((soldierypos + 20) > Sprite_YPos) && (soldierxpos-16 >  Sprite_XPos)) //bottom right
				begin
					soldierxmotion <= ~soldierxstep + 1'b1;					
					soldierymotion <= ~soldierystep + 1'b1;				
				end
				else if(((soldierypos + 20) > Sprite_YPos) && (soldierxpos-16 < Sprite_XPos)) //bottom left
				begin
					soldierxmotion <= soldierxstep;					
					soldierymotion <= ~soldierystep + 1'b1;				
				end				
				else if(((soldierypos - 20) < Sprite_YPos) && (soldierxpos-16 == Sprite_XPos)) //top
				begin
					soldierxmotion <= 11'd0;					
					soldierymotion <= soldierystep;
				end	
				else if((soldierxpos-16 > Sprite_XPos) && (soldierypos == Sprite_YPos)) //right
				begin
					soldierxmotion <= ~soldierxstep + 1'b1;					
					soldierymotion <= 11'd0;
				end					
				else if(((soldierypos + 20) > Sprite_YPos) && (soldierxpos-16 == Sprite_XPos)) //bottom
				begin
					soldierxmotion <= 11'd0;					
					soldierymotion <= ~soldierystep + 1'b1;					
				end		
				else if((soldierxpos-16 < Sprite_XPos) && (soldierypos  == Sprite_YPos)) //left
				begin
					soldierxmotion <= soldierxstep;					
					soldierymotion <= 11'd0;
				end					
				else //same position
				begin
					soldierxmotion <= 11'd0;
					soldierymotion <= 11'd0;
				end

/*********************************UPDATE POSITION**********************************/				 
				 Sprite_YPos <= (Sprite_YPos + Sprite_YMotion); 
				 Sprite_XPos <= (Sprite_XPos + Sprite_XMotion);

				 soldierypos <= (soldierypos + soldierymotion);  
				 soldierxpos <= (soldierxpos + soldierxmotion);				 

		end  
    end
       
    assign spritex = Sprite_XPos;
   
    assign spritey = Sprite_YPos;
	 
	 assign soldierx = soldierxpos;
	 
	 assign soldiery = soldierypos;
    

endmodule