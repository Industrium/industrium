module spritestate(input Clk, Reset, playerdeath, //I assume the clock is the vertical sync? So that the next sprite is drawn?
						input [15:0] Keycode, //keyboard input
						output logic [6:0] motion, //how the sprite will move
						output logic [1:0] hitvalid					
				   );
						
/* This module is for taking the selection signal and making sure that the correct motion
* is taken for the sprite. For instance the walking right animation is 4 spirtes
* so we need a way to make it cycle through those 4 before repeating.		   
*/
enum logic[4:0] {LEFTMOVE, LEFTMOVE2, LEFTMOVE3, LEFTMOVE4, LEFTMOVE5, LEFTMOVE6,
					RIGHTMOVE, RIGHTMOVE2, RIGHTMOVE3, RIGHTMOVE4, RIGHTMOVE5, RIGHTMOVE6,
					LATTACK1, LATTACK2, LATTACK3, LATTACK4,
					RATTACK1, RATTACK2, RATTACK3, RATTACK4, 
					/*JUMPLEFT, JUMPRIGHT,*/ LEFTDEATH, RIGHTDEATH,	//Jump 
					RWAIT, LWAIT} curr_state, next_state; //ADD ATTACK ANIMATIONS TOO!!!
									
/* LWAIT and RWAIT are used to keep track of the sprite direction. For instance when 
* we're going right, and we're not pressing the right arrow, then we want
* the sprite to face right. Vice versa for left
*/

//Internal state logic
//Assign "next_state based on "state" and "Execute"
logic [1:0] counter; //counter to slow draw so that character moves fluidly
logic [2:0] count;

always_ff@(posedge Clk or posedge Reset)
	begin 
		if(Reset)
		begin
			curr_state <= RWAIT; //default to looking right
			counter <= 0;
			count <= 0;
		end
		else if(curr_state == RIGHTDEATH || curr_state == LEFTDEATH)
		begin
			count <= count+1;
			counter <= counter + 1;
			if(count == 3'b111)
				count <= 0;
		end
		else if (counter == 2'b11)
			begin
				curr_state <= next_state;
				counter <= 0;
			end
		else
			counter <= counter + 1;
	end

		//Assign outputs based on "state"
always_comb
	begin
		next_state = curr_state; //Guarantees that it is replaced

/*******************Define state transition***************************/

		case(curr_state)
			RWAIT: 
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051) //right move or move up or move down
						next_state = RIGHTMOVE;
					else if (Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE;
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;
//					else if (Keycode == 'd44)
//						next_state = JUMPRIGHT;						
					else
						next_state = RWAIT;
				end
				
			LWAIT: 
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;				
					else if (Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;
//					else if (Keycode == 'd44)
//						next_state = JUMPLEFT;							
					else
						next_state = LWAIT;
				end						

/*************************RIGHT************************************/

			RIGHTMOVE:
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE2;
					else if(Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE;	
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;							
					else
						next_state = RWAIT;
				end

			RIGHTMOVE2: 
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE3;
					else if(Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE2;	
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;							
					else
						next_state = RWAIT;
				end
							  
			RIGHTMOVE3:						
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE4;
					else if(Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE3;		
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;							
					else
						next_state = RWAIT;
				end

			RIGHTMOVE4:						
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE5;
					else if(Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE4;	
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;							
					else
						next_state = RWAIT;
				end

			RIGHTMOVE5:						
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE6;
					else if(Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE5;		
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;							
					else
						next_state = RWAIT;
				end				

			RIGHTMOVE6:						
				begin
					if(playerdeath == 1'b1)
						next_state = RIGHTDEATH;				
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE;
					else if(Keycode == 'h0050 || 
								Keycode == 'h5250 || 
								Keycode == 'h5052 || 
								Keycode == 'h0052 || 
								Keycode == 'h5150 || 
								Keycode == 'h5051 || 
								Keycode == 'h0051)
						next_state = LEFTMOVE6;		
					else if (Keycode == 'h001B || Keycode == 'h1B4f)
						next_state = RATTACK1;							
					else
						next_state = RWAIT;
				end					
/**************************LEFT*************************/					

			LEFTMOVE:
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;						
					else if(Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE2;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;						
					else
						next_state = LWAIT;
				end

			LEFTMOVE2: 
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;						
					else if(Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE3;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE2;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;						
					else
						next_state = LWAIT;
				end
							  
			LEFTMOVE3:						
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;						
					else if(Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE4;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE3;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;						
					else
						next_state = LWAIT;
				end	
	
			LEFTMOVE4:						
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;						
					else if(Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE5;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE4;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;						
					else
						next_state = LWAIT;
				end
	
			LEFTMOVE5:						
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;						
					else if(Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE6;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE5;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;						
					else
						next_state = LWAIT;
				end		

			LEFTMOVE6:						
				begin
					if(playerdeath == 1'b1)
						next_state = LEFTDEATH;						
					else if(Keycode == 'h0050 || 
						Keycode == 'h5250 || 
						Keycode == 'h5052 || 
						Keycode == 'h0052 || 
						Keycode == 'h5150 || 
						Keycode == 'h5051 || 
						Keycode == 'h0051)
						next_state = LEFTMOVE;
					else if(Keycode == 'h004f || 
						Keycode == 'h524f || 
						Keycode == 'h4f52 || 
						Keycode == 'h0052 || 
						Keycode == 'h514f || 
						Keycode == 'h4f51 || 
						Keycode == 'h0051)
						next_state = RIGHTMOVE6;
					else if (Keycode == 'h001B || Keycode == 'h1B50)
						next_state = LATTACK1;						
					else
						next_state = LWAIT;
				end	
				
/**************************LATTACK*************************/					

			LATTACK1:
			begin
			if(playerdeath == 1'b1)
				next_state = LEFTDEATH;
			else
				next_state = LATTACK2;	
			end			
			LATTACK2:
			begin
			if(playerdeath == 1'b1)
				next_state = LEFTDEATH;
			else
				next_state = LATTACK3;	
			end				
			LATTACK3:
			begin
			if(playerdeath == 1'b1)
				next_state = LEFTDEATH;
			else
				next_state = LATTACK4;	
			end				
			LATTACK4: 
			begin 
				if(playerdeath == 1'b1)
					next_state = LEFTDEATH;			
				else if(Keycode == 'h001B || Keycode == 'h1B50)
					next_state = LATTACK1;
				else
					next_state = LWAIT;		
			end
/**************************RATTACK*************************/	

			RATTACK1: 
			begin
			if(playerdeath == 1'b1)
				next_state = RIGHTDEATH;
			else
				next_state = RATTACK2;	
			end
			RATTACK2:
			begin
			if(playerdeath == 1'b1)
				next_state = RIGHTDEATH;
			else
				next_state = RATTACK3;	
			end			
			RATTACK3:
			begin
			if(playerdeath == 1'b1)
				next_state = RIGHTDEATH;
			else
				next_state = RATTACK4;	
			end			
			RATTACK4:
			begin 
				if(playerdeath == 1'b1)
					next_state = RIGHTDEATH;		
				else if(Keycode == 'h001B || Keycode == 'h1B4f)
					next_state = RATTACK1;
				else
					next_state = RWAIT;		
			end			
			
/**************************Jump*************************/			
//			JUMPLEFT: 
//			begin
//				if(playerdeath == 1'b1)
//					next_state = LEFTDEATH;			
//				else if(hop == 1) 
//					next_state = JUMPLEFT;
//				else
//					next_state = LWAIT;
//			end
//
//			JUMPRIGHT:
//			begin
//				if(playerdeath == 1'b1)
//					next_state = RIGHTDEATH;				
//				else if(hop == 1) 
//					next_state = JUMPRIGHT;
//				else
//					next_state = RWAIT;
//			end			

/*******************************DEATH**********************************/			
			LEFTDEATH:
			begin
				if(count == 3'b111)
					next_state = LWAIT;
				else
					next_state = LEFTDEATH;
			end
			
			RIGHTDEATH:
			begin
				if(count == 3'b111)
					next_state = RWAIT;
				else
					next_state = RIGHTDEATH;
			end			
			default: ;
		endcase
			
		

		hitvalid = 2'b00;
/*************************Define signals******************************/		
		case(curr_state)
			RWAIT: motion = 6'b000110; //facing right rest position (SAME AS WALKING)	
			LWAIT: motion = 6'b000101; //facing left rest position
			
/*************************RIGHT************************************/		
			RIGHTMOVE: motion = 6'b010010; //18
			RIGHTMOVE2: motion = 6'b010011; //19	  
			RIGHTMOVE3: motion = 6'b010100;	//20	
			RIGHTMOVE4: motion = 6'b010101;	//21
			RIGHTMOVE5: motion = 6'b010110;	//22
			RIGHTMOVE6: motion = 6'b010111;	//23	

/*****************LEFT*************************/							
			LEFTMOVE6: motion = 6'b001100; //12
			LEFTMOVE5: motion = 6'b001101; //13	
			LEFTMOVE4: motion = 6'b001110; //14				
			LEFTMOVE3: motion = 6'b001111; //15	
			LEFTMOVE2: motion = 6'b010000; //16	
			LEFTMOVE:  motion = 6'b010001; //17
			
/*****************LATTACK**********************/
			LATTACK1: motion = 6'b000011; //0
			LATTACK2: motion = 6'b000010; //1
			LATTACK3: 
			begin
				motion = 6'b000001; //2
				hitvalid = 2'b01;
			end
			LATTACK4: motion = 6'b000000;	//3
/*****************RATTACK**********************/			
			RATTACK1: motion = 6'b001000; //8
			RATTACK2: motion = 6'b001001; //9
			RATTACK3: 
			begin
				motion = 6'b001010; //10
				hitvalid = 2'b10;
			end
			RATTACK4: motion = 6'b001011;	//11
/******************LEFTJUMP********************/
//			JUMPLEFT: motion = 6'b000100;  //4
/******************RIGHTJUMP********************/			
//			JUMPRIGHT: motion = 6'b000111; //7
			
/*******************DEATH*************************/
			LEFTDEATH: motion = 6'd24;
			RIGHTDEATH: motion = 6'd25;
			default: ;
		endcase
	end
	
endmodule