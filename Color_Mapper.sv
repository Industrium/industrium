module  color_mapper (	input Clk, Reset,
								input [10:0] DrawX, DrawY,
								input [15:0] GAMEDATA,
								input [7:0] buffer2out,
								input switch,
								output logic [7:0]  Red, Green, Blue,
								output logic [19:0] drawaddr,
								output logic [18:0] drawbuff2addr,	
								output logic sramread, read, SRAM_OE_N
								);			
	
	always_ff@(posedge Clk or posedge Reset)
	begin //READ FRAMEBUFFER
		if(Reset)
		begin	
			sramread <= 1'b0;
			read <= 1'b0;
			drawaddr <= 0;
			SRAM_OE_N <= 1'b1;
			drawbuff2addr <= 0;
		end
		else if ((DrawX >= 0) && (DrawX < 640) &&
				(DrawY >= 0) && (DrawY < 480))
			begin
				case(switch) //which buffer to read from
				1'b1: //sram buffer is not writing 
				begin
					sramread <= 1'b1;
					read <= 1'b0;
					SRAM_OE_N <= 1'b0;
					drawaddr <= DrawX+640*DrawY;
					drawbuff2addr <= 0;
				end
				1'b0: //bram buffer is not writing
				begin
					read <= 1'b1;
					sramread <= 1'b0; //for timing
					SRAM_OE_N <= 1'b1;
					drawbuff2addr <= DrawX+640*DrawY;
					drawaddr <= 0;
				end
				endcase
			end
		else
		begin
			sramread <= 1'b0;
			read <= 1'b0;
			drawaddr <= 0;
			SRAM_OE_N <= 1'b1;
			drawbuff2addr <= 0;
		end
	end
	
	always_comb
    begin //RGB DISPLAY
		  case(switch)
			1'b1:
			begin
			  Red = {GAMEDATA[15:13], 5'b00000};
			  Green = {GAMEDATA[12:10], 5'b00000};
			  Blue = {GAMEDATA[9:8], 6'b000000};
			end
			1'b0: 
			begin
			  Red = {buffer2out[7:5], 5'b00000};
			  Green = {buffer2out[4:2], 5'b00000};
			  Blue = {buffer2out[1:0], 6'b000000};			
			end
		  endcase
    end 							
															
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
/* Our modified color_mapper is used to display our sprites from ROM,
 * which contains information about the RGB values to display.
 */
 
//	logic shape_on; //draw sprite or not. May need more logic for multiple layers.
//	logic [10:0] shape_x_size = 32; //specifically for the steampunk sprite.
//	logic [10:0] shape_y_size = 52; //Need more sizes for choosing between different sized sprites.	 
//	 
//	logic[14:0] address; 
//	spriterom R0M(	.addr(address), //instantiated ROM containing sprite 
//								.q(data)
//								);

//	logic spriteon, backgroundon; //draw sprite or not. May need more logic for multiple layers.
//	logic [10:0] shapexsize = 140;
//	logic [10:0] shapeysize = 124;
//	logic [10:0] solxsize = 70;
//	logic [10:0] solysize = 80;	
//	logic [10:0] shape_x_size = 640;
//	logic [10:0] shape_y_size = 480;
//	
//    always_comb
//    begin: sprites	 
//        if ((DrawX >= 0) && (DrawX < shape_x_size) &&
//				(DrawY >= 0) && (DrawY < shape_y_size)&& !((DrawX >= shape_x) && (DrawX < (shape_x+shapexsize)) &&
//				(DrawY >= shape_y) && (DrawY < (shape_y+shapeysize))) && !((DrawX >= 520) && (DrawX < (520+solxsize)) &&
//				(DrawY >= 344) && (DrawY < (344+solysize))))
//			begin	
//            backgroundon = 1'b1;
//				spriteon = 1'b0;
//				address = 0;				
//				ADDR = DrawX+640*DrawY;
//			end
//		  else if((DrawX >= shape_x) && (DrawX < (shape_x+shapexsize)) &&
//				(DrawY >= shape_y) && (DrawY < (shape_y+shapeysize)) && !((DrawX >= 520) && (DrawX < (520+solxsize)) &&
//				(DrawY >= 344) && (DrawY < (344+solysize))))
//			begin
//				spriteon = 1'b1;
//				backgroundon = 1'b0;
//				ADDR = DrawX+640*DrawY;				
//					case(sel)							
//						6'b000101: address = (840*(DrawY-shape_y) + (DrawX-shape_x+140)); //LREST/WAIT
//						6'b001100: address = (840*(DrawY-shape_y) + (DrawX-shape_x)); //LMOVE6
//						6'b001101: address = (840*(DrawY-shape_y) + (DrawX-shape_x+140)); //LMOVE5
//						6'b001110: address = (840*(DrawY-shape_y) + (DrawX-shape_x+280)); //LMOVE4
//						6'b001111: address = (840*(DrawY-shape_y) + (DrawX-shape_x+420)); //LMOVE3
//						6'b010000: address = (840*(DrawY-shape_y) + (DrawX-shape_x+560)); //LMOVE2
//						6'b010001: address = (840*(DrawY-shape_y) + (DrawX-shape_x+700)); //LMOVE
//						
//						6'b000110: address = (840*(DrawY-shape_y) + (DrawX-shape_x+560))+208320; //REST/WAIT
//						6'b010010: address = (840*(DrawY-shape_y) + (DrawX-shape_x))+208320; //RMOVE
//						6'b010011: address = (840*(DrawY-shape_y) + (DrawX-shape_x+140))+208320; //RMOVE2
//						6'b010100: address = (840*(DrawY-shape_y) + (DrawX-shape_x+280))+208320; //RMOVE3
//						6'b010101: address = (840*(DrawY-shape_y) + (DrawX-shape_x+420))+208320; //RMOVE4
//						6'b010110: address = (840*(DrawY-shape_y) + (DrawX-shape_x+560))+208320; //RMOVE5
//						6'b010111: address = (840*(DrawY-shape_y) + (DrawX-shape_x+700))+208320; //RMOVE6
//					
//					6'b000011: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+420)); //LATTACK1						
//					6'b001000: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+280))+208320; //RATTACK1					
//					6'b000010: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+280)); //LATTACK2	
//					6'b001001: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+420))+208320; //RATTACK2						
//					6'b000000: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+140)); //LATTACK3
//					6'b000001: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x)); //LATTACK4						
//					6'b001010: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+560))+208320; //RATTACK3
//					6'b001011: address = (840*(DrawY-shape_y+123) + (DrawX-shape_x+700))+208320; //RATTACK4							
//						default: address = 0;
//					endcase	
//			end
//		  else if((DrawX >= 520) && (DrawX < (520+solxsize)) &&
//				(DrawY >= 344) && (DrawY < (344+solysize)))
//			begin		
//				spriteon = 1'b1;
//				backgroundon = 1'b0;	
//				ADDR = DrawX+640*DrawY;					
//				address = (70*(DrawY-344) + (DrawX-520))+416640;
//			end				
//			else
//				begin
//					backgroundon = 0;
//					spriteon = 0;		
//					address = 0;
//					ADDR = 0;
//				end			
//     end 
	  
	  /**
	  * USE sramread FROM MEMORYCONTROLLER HERE
	  *
	  */
	  
	  
//	 logic [15:0] data; 
//	 always_comb
//	 begin: endianfix
//		data = {spritedata[7:0] , spritedata[15:8]};
//    end	
	  
//    always_comb
//    begin:RGB_Display
//        if ((backgroundon == 1'b1)) //data is not trivial
//        begin 
////            Red = {spritedata[15:11], 3'b000};
////            Green = {spritedata[10:5], 2'b00};
////            Blue = {spritedata[4:0], 3'b000};
//				  Red = {backgrounddata[7:5], 5'b00000};
//				  Green = {backgrounddata[4:2], 5'b00000};
//				  Blue = {backgrounddata[1:0], 6'b000000};
//        end       
//        else if((spriteon == 1'b1))
//        begin 
////            Red = (data!=8'h00) ? {data[7:5], 5'b00000} : {spritedata[15:11], 3'b000};
////            Green = (data!=8'h00) ? {data[4:2], 5'b00000} : {spritedata[10:6], 3'b000};
////            Blue = (data!=8'h00) ? {data[1:0], 6'b000000} : {spritedata[4:0], 3'b000};
//            Red = (data!=16'h0326 && data!=16'h5362) ? {data[15:11], 3'b000} : {backgrounddata[7:5], 5'b00000};
//            Green = (data!=16'h0326 && data!=16'h5362) ? {data[10:6], 3'b000} : {backgrounddata[4:2], 5'b00000};
//            Blue = (data!=16'h0326 && data!=16'h5362) ? {data[5:1], 3'b000} : {backgrounddata[1:0], 6'b000000};
//        end  
//		  else 
//			begin
//            Red = 8'hFF;
//            Green = 8'hFF;
//            Blue = 8'hFF;
//			end
//    end 
    
endmodule