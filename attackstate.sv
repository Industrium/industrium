module attackstate(input Clk, Reset, //I assume the clock is the vertical sync? So that the next sprite is drawn?
					input [7:0] Keycode, //keyboard input
					output logic [3:0] motion //how the sprite will move
				   );
						
/* This module is for taking the selection signal and making sure that the correct motion
* is taken for the sprite. For instance the walking right animation is 4 spirtes
* so we need a way to make it cycle through those 4 before repeating.		   
* THIS IS ONLY FOR THE STEAMPUNK FIGURE. But can be modified for any sprite movement.
* SPAZZES OUT TOO MUCH
*/
enum logic[3:0] {LEFTATTACK1, LEFTATTACK2, LEFTATTACK3, LEFTATTACK4, 
				 RIGHTATTACK4, RIGHTATTACK2, RIGHTATTACK3, RIGHTATTACK1, LWAIT, RWAIT} curr_state, next_state;
									
/* LWAIT and RWAIT are used to keep track of the sprite direction. For instance when 
* we're going right, and we're not pressing the right arrow, then we want
* the sprite to face right. Vice versa for left
*/

//Internal state logic
//Assign "next_state based on "state" and "Execute"
logic [1:0] counter = 0; //counter to slow draw so that character moves fluidly

always_ff@(posedge Clk or posedge Reset)
	begin 
		if(Reset)
			curr_state <= RWAIT; //default to looking right
		else if (counter == 2'b11)
			begin
				curr_state <= next_state;
				counter <= 0;
			end
		else
			counter <= counter + 1;
	end

		//Assign outputs based on "state"
always_comb
	begin
		next_state = curr_state; //Guarantees that it is replaced

/*******************Define state transition***************************/

		case(curr_state)
			RWAIT: 
				begin
					if(Keycode == 'd27)
						next_state = RIGHTATTACK1;
					else
						next_state = RWAIT;
				end
				
			LWAIT: 
				begin
					if(Keycode == 'd27)
						next_state = LEFTATTACK1;
					else
						next_state = LWAIT;
				end						

/*************************RIGHTATTACK************************************/
			RIGHTATTACK1: next_state = RIGHTATTACK2;
			RIGHTATTACK2: next_state = RIGHTATTACK3;
			RIGHTATTACK3: next_state = RIGHTATTACK4;

/**************************LEFTATTACK*************************/					
			LEFTATTACK1: next_state = LEFTATTACK2;
			LEFTATTACK2: next_state = LEFTATTACK3;
			LEFTATTACK3: next_state = LEFTATTACK4;	
				
		endcase

/*************************Define signals******************************/		
		case(curr_state)
			RWAIT: motion = 4'b1000; //facing right rest position				
			LWAIT: motion = 4'b0100; //facing left rest position
			
/*************************RIGHT************************************/
			RIGHTREST: motion = 4'b1000; //movements right
			RIGHTMOVE: motion = 4'b1001;
			RIGHTREST2: motion = 4'b1010;	  
			RIGHTMOVE2: motion = 4'b1011;					

/*****************LEFT*************************/					
			LEFTREST: motion = 4'b0100; //movements left
			LEFTMOVE: motion = 4'b0101;
			LEFTREST2: motion = 4'b0110; 							  
			LEFTMOVE2: motion = 4'b0111;					
		endcase
	end
endmodule				
				
