module framebuffer(input Clk, 
								Reset_h, 
							   disappear,	
								transparency,
						input [3:0] samsel, skysel,
						input [6:0] sel, enemyselect,
						input [1:0] special, hitvalid, skyhitvalid,
						input [9:0] shape_x, DrawX, soldierx,//choose x and y to draw at a certain location
						input [9:0] shape_y, DrawY, soldiery,//probably from a sprite.sv helper
						input [7:0] FL_DQ,
						output logic [22:0] FL_ADDR,	//to flash	
						output logic FL_OE_N,
										 FL_CE_N,
										 SRAM_WE_N,
										 SRAM_CE_N,
										 writesig, //ready to write		
										 switch, //which buffer to write to				
						output [2:0] enemyhit, playerhit,								 
						output [7:0] buffer2data,
						output [15:0] data,	 //data to SRAM2IO, GAMEDATA to colormapper 
						output [19:0] copyaddr,
						output [18:0] buffer2addr
								);
								
								
logic [15:0] parseddata;
logic [7:0] bf2data;
logic [7:0] FLASHDATA;
logic transfer; //outputs
logic sramwrite, flread; //controlled by dataparser?

/****************************************DATA REGISTERS**************************/
Register #(.N(8)) flashreg(.Clk(Clk), //all hold data until stable
				.Reset(Reset_h), 
				.Transfer(transfer),
            .Reg_In(FL_DQ),
            .Reg_Out(FLASHDATA)
				);
								
Register #(.N(16)) sramw(.Clk(Clk), 
				.Reset(Reset_h), 
				.Transfer(writesig),
            .Reg_In(parseddata),
            .Reg_Out(data)
				);								

Register #(.N(8)) sramfb2(.Clk(Clk), 
				.Reset(Reset_h), 
				.Transfer(writesig),
            .Reg_In(bf2data),
            .Reg_Out(buffer2data)
				);					
/**************************MEMORY CONTROL*************************************/						
memorycontroller memstate(.Clk(Clk), 
								  .Reset(Reset_h),
							     .flread(flread),
								  .switch(switch),
								  .FL_OE_N(FL_OE_N), 
								  .FL_CE_N(FL_CE_N),
								  .transfer(transfer),
								  .SRAM_WE_N(SRAM_WE_N), 
								  .writesig(writesig),
								  .SRAM_CE_N(SRAM_CE_N)
								);

/**********************************BLITTER**********************************************/								
dataparser getdata(.Clk(Clk), 
						 .Reset(Reset_h),
						 .sel(sel),
						 .special(special),
						 .enemyselect(enemyselect),
						 .hitvalid(hitvalid),
						 .skyhitvalid(skyhitvalid),
						 .samsel(samsel), 
						 .skysel(skysel),
						 .writesig(writesig),
				 	    .disappear(disappear),						 
						 .FLASHDATA(FLASHDATA),
						 .shape_x(shape_x), //choose x and y to draw at a certain location
						 .shape_y(shape_y), //probably from a sprite.sv helper
					    .soldierx(soldierx), 
					    .soldiery(soldiery),		
						 .DrawX(DrawX),
						 .DrawY(DrawY),
						 .ADDR(FL_ADDR),	//to flash
						 .copyaddr(copyaddr),
						 .buffer2addr(buffer2addr),
						 .buffer2data(bf2data),
						 .flread(flread),
						 .parseddata(parseddata),
						 .switch(switch),
						 .enemyhit(enemyhit),
						 .playerhit(playerhit),
						 .transparent(transparency)
						);				
													
	
endmodule