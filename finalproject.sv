module  finalproject 		( input         CLOCK_50,
                       input[3:0]    KEY, //bit 0 is set up as Reset
							  output [6:0]  HEX0, HEX1, HEX2, HEX3, //HEX4, HEX5, HEX6, HEX7,
							  //output [8:0]  LEDG,
							  //output [17:0] LEDR,
							  // VGA Interface 
                       output [7:0]  VGA_R,					//VGA Red
							                VGA_G,					//VGA Green
												 VGA_B,					//VGA Blue
							  output        VGA_CLK,				//VGA Clock
							                VGA_SYNC_N,			//VGA Sync signal
												 VGA_BLANK_N,			//VGA Blank signal
												 VGA_VS,					//VGA virtical sync signal	
												 VGA_HS,					//VGA horizontal sync signal
							  // CY7C67200 Interface
							  inout [15:0]  OTG_DATA,						//	CY7C67200 Data bus 16 Bits
							  output [1:0]  OTG_ADDR,						//	CY7C67200 Address 2 Bits
							  output        OTG_CS_N,						//	CY7C67200 Chip Select
												 OTG_RD_N,						//	CY7C67200 Write
												 OTG_WR_N,						//	CY7C67200 Read
												 OTG_RST_N,						//	CY7C67200 Reset
							  input			 OTG_INT,						//	CY7C67200 Interrupt
							  // SDRAM Interface for Nios II Software
							  output [12:0] DRAM_ADDR,				// SDRAM Address 13 Bits
							  inout [31:0]  DRAM_DQ,				// SDRAM Data 32 Bits
							  output [1:0]  DRAM_BA,				// SDRAM Bank Address 2 Bits
							  output [3:0]  DRAM_DQM,				// SDRAM Data Mast 4 Bits
							  output			 DRAM_RAS_N,			// SDRAM Row Address Strobe
							  output			 DRAM_CAS_N,			// SDRAM Column Address Strobe
							  output			 DRAM_CKE,				// SDRAM Clock Enable
							  output			 DRAM_WE_N,				// SDRAM Write Enable
							  output			 DRAM_CS_N,				// SDRAM Chip Select
							  output			 DRAM_CLK,				// SDRAM Clock
							  
							  //SRAM for Frame Buffer
							  output logic [19:0] SRAM_ADDR,				// SRAM Address 20 Bits
							  inout wire [15:0]  SRAM_DQ,				// SRAM Data 16 Bits
							  output	logic		 SRAM_UB_N,			// SRAM Row Address Strobe
							  output	logic		 SRAM_LB_N,			// SRAM Column Address Strobe
							  output	logic		 SRAM_CE_N,				// SRAM Clock Enable
							  output	logic		 SRAM_OE_N,				// SRAM Output Enable
							  output	logic		 SRAM_WE_N,				// SRAM Write Enable
							  
							  //FLASH
							  	inout [7:0] FL_DQ,
								output [22:0] FL_ADDR,
								output FL_CE_N,
								output FL_OE_N,
								output FL_WE_N,
								output FL_RST_N,
								output FL_WP_N,
								input FL_RY
											);
    
    logic Reset_h, vssig, Clk, spriteclk;
    logic [10:0] drawxsig, drawysig, spritexsig, spriteysig, soldierxsig, soldierysig;
	 logic [6:0] select, enemyselect;
	 logic [15:0] keycode;
    
	 assign Clk = CLOCK_50;
    assign {Reset_h}=~ (KEY[0]);  // The push buttons are active low
	
	 
	 wire [1:0] hpi_addr;
	 wire [15:0] hpi_data_in, hpi_data_out;
	 wire hpi_r, hpi_w,hpi_cs;

/************************************OTG INTERFACE************************/	 
	 hpi_io_intf hpi_io_inst(   .from_sw_address(hpi_addr),
										 .from_sw_data_in(hpi_data_in),
										 .from_sw_data_out(hpi_data_out),
										 .from_sw_r(hpi_r),
										 .from_sw_w(hpi_w),
										 .from_sw_cs(hpi_cs),
		 								 .OTG_DATA(OTG_DATA),    
										 .OTG_ADDR(OTG_ADDR),    
										 .OTG_RD_N(OTG_RD_N),    
										 .OTG_WR_N(OTG_WR_N),    
										 .OTG_CS_N(OTG_CS_N),    
										 .OTG_RST_N(OTG_RST_N),   
										 .OTG_INT(OTG_INT),
										 .Clk(Clk),
										 .Reset(Reset_h)
	 );
	 
	 /*****************************************NIOS SYSTEM*******************************/
	 nios_system nios_system(
										 .clk_clk(Clk),         
										 .reset_reset_n(KEY[0]),   
										 .sdram_wire_addr(DRAM_ADDR), 
										 .sdram_wire_ba(DRAM_BA),   
										 .sdram_wire_cas_n(DRAM_CAS_N),
										 .sdram_wire_cke(DRAM_CKE),  
										 .sdram_wire_cs_n(DRAM_CS_N), 
										 .sdram_wire_dq(DRAM_DQ),   
										 .sdram_wire_dqm(DRAM_DQM),  
										 .sdram_wire_ras_n(DRAM_RAS_N),
										 .sdram_wire_we_n(DRAM_WE_N), 
										 .sdram_clk_clk(DRAM_CLK), //changed
										 .keycode_export(keycode),  
										 .otg_hpi_address_export(hpi_addr),
										 .otg_hpi_data_in_port(hpi_data_in),
										 .otg_hpi_data_out_port(hpi_data_out),
										 .otg_hpi_cs_export(hpi_cs),
										 .otg_hpi_r_export(hpi_r),
										 .otg_hpi_w_export(hpi_w));
	
/****************************************VGA CONTROLLER******************************/	
    vga_controller vgasync_instance(.*,
												.Reset(Reset_h),
												.hs(VGA_HS),        // Horizontal sync pulse.  Active low
								            .vs(VGA_VS),        // Vertical sync pulse.  Active low
												.pixel_clk(VGA_CLK), // 25 MHz pixel clock output
												.blank(VGA_BLANK_N),     // Blanking interval indicator.  Active low.
												.sync(VGA_SYNC_N),      // Composite Sync signal.  Active low.  We don't use it in this lab, but the video DAC on the DE2 board requires an input for it.
												.DrawX(drawxsig),     // horizontal coordinate
								            .DrawY(drawysig)
												);
   
	 assign vssig = VGA_VS;

	/********************************SPRITE MOTIONS***********************************/ 
	logic [10:0] soldierxmotion, soldierymotion, soldierxpos, 
					soldierypos, spritexpos, spriteypos, spritexmotion;
	logic [1:0] special; 
	logic disappear, enemydeath, playerdeath, transparency; 
	logic [3:0] skysel, samsel;
	logic [2:0] enemyhit, playerhit; //7 diff
	logic [1:0] hitvalid, skyhitvalid; //01 is Lhit 10 is Rhit
	logic [1:0] enemyhitvalid; //01 is lhit, 10 is rhit
	
    sprite_controller sprite(.Reset(Reset_h), 
							  .frame_clk(vssig), //clocked on VSYNC as detailed in vga tutorial
							  .Keycode(keycode),
							  .motion(enemyselect),
							  .enemydeath(enemyhit),
							  .playerdeath(playerhit),
							  .spritex(spritexsig), 
							  .spritey(spriteysig),
							  .soldierx(soldierxsig), 
							  .soldiery(soldierysig),
							  .soldierxmotion(soldierxmotion),
							  .soldierymotion(soldierymotion),
							  .soldierxpos(soldierxpos), 
							  .soldierypos(soldierypos),	
							  .Sprite_XPos(spritexpos), 
							  .Sprite_YPos(spriteypos)					  
							  );
							  
	 spritestate states(.Clk(vssig), //theoretically only every frame creation should prompt change in motion 
							  .Reset(Reset_h),
							  .playerdeath(playerhit),
							  .Keycode(keycode), //keyboard input
						     .motion(select), //how the sprite will move
							  .hitvalid(hitvalid)
							  );		
			
	 enemymoveset state(.Clk(vssig), //theoretically only every frame creation should prompt change in motion 
							  .Reset(Reset_h),
							  .special(special),
							  .enemydeath(enemyhit),
							  .soldierxmotion(soldierxmotion),
							  .soldierymotion(soldierymotion),
							  .soldierxpos(soldierxpos), 
							  .soldierypos(soldierypos),	
							  .spritexpos(spritexpos), 
							  .spriteypos(spriteypos),								  
						     .motion(enemyselect), //how the sprite will move
							  .disappear(disappear),
							  .skyhitvalid(skyhitvalid),
							  .transparency(transparency)
							  );		
						
//	 healthstate hs(.Clk(vssig), 
//						 .Reset(Reset_h), //I assume the clock is the vertical sync? So that the next sprite is drawn?
//						 .playerhit(playerhit),					
//						 .samsel(samsel), 
//						 .playerdeath(playerdeath)
//						);		
//		
//	skyhealth skh(.Clk(vssig), 
//						 .Reset(Reset_h), //I assume the clock is the vertical sync? So that the next sprite is drawn?
//						 .enemyhit(enemyhit), 
//						 .skysel(skysel),
//						 .enemydeath(enemydeath) 
//						 );	
							  
   /*Possible timing issue between spritestatae and sprite_controller?
	* Ideally we need both motion and spritex and spritey to be coordinated 
	* so that color_mapper can map the correct color.
	*/
	assign	SRAM_UB_N = 1'b0;			// SRAM Upper Byte
	assign	SRAM_LB_N = 1'b0;			// SRAM Lower Byte
//	assign	SRAM_CE_N = 1'b0;				// SRAM Clock Enable	
//	assign   SRAM_WE_N = 1'b1; //never write
//	assign	SRAM_OE_N = 1'b0;				// SRAM Output Enable

	//assign FL_RY = 1'b1;
	assign FL_WE_N = 1'b1; //never write
	assign FL_RST_N = 1'b1;
	assign FL_WP_N = 1'b1;
//	assign FL_OE_N = 1'b0;
//	assign FL_CE_N = 1'b0;

logic [15:0] SRAMREADDATA, SRAMWRITEDATA, GAMEDATA;
logic [15:0] framedata, data;
logic [19:0] drawaddr, copyaddr;
logic [18:0] drawbuff2addr;
logic sramread, writesig, switch;

/*****************************************FRAME BUFFER*************************************/
logic [18:0] buffer2addr;
logic [7:0] buffer2data;

framebuffer FB(.Clk(Clk), 
					.Reset_h(Reset_h), 
					.sel(select),
					.special(special),
					.enemyselect(enemyselect),
					.hitvalid(hitvalid),
					.skyhitvalid(skyhitvalid),
					.shape_x(spritexsig), //choose x and y to draw at a certain location
					.shape_y(spriteysig), //probably from a sprite.sv helper
				   .soldierx(soldierxsig), 
				   .soldiery(soldierysig),	
					.DrawX(drawxsig), //choose x and y to draw at a certain location
					.DrawY(drawysig), //probably from a sprite.sv helper					
					.FL_DQ(FL_DQ),
					.FL_ADDR(FL_ADDR),	//to flash	
					.copyaddr(copyaddr),
					.buffer2addr(buffer2addr),
					.buffer2data(buffer2data),
					.switch(switch),
					.enemyhit(enemyhit),
					.playerhit(playerhit),
				   .samsel(samsel), 
				   .skysel(skysel),					
					.FL_OE_N(FL_OE_N),
					.SRAM_WE_N(SRAM_WE_N),
					.SRAM_CE_N(SRAM_CE_N),
					.FL_CE_N(FL_CE_N),
					.data(data),	
					.writesig(writesig),
				   .disappear(disappear),
					.transparency(transparency)
						);
						
/*******************************SECOND FRAME BUFFER********************************/
logic [18:0] fbaddr;
logic [7:0] buffer2out;
logic write, read;					
					
framebuffer2 fb2(
	.wraddress(fbaddr), //address to be inserted.
	.rdaddress(drawbuff2addr),
	.rden(read),
	.data(buffer2data), //data to be inserted
	.clock(Clk),
	.wren(write), //ready to write
	.q(buffer2out)
	);			

/*******************************framearbiter***********************************/
framearbiter FA(.*);	//determines which buffer to write to

/***************SRAM Controllers**************************************/						
tristate #(.N(16)) tr0(
							.Clk(Clk), 
							.OE(~SRAM_WE_N),
							.In(SRAMWRITEDATA), 
							.Out(SRAMREADDATA), 
							.SRAM_DQ(SRAM_DQ)
								);
								

								
SRAM2IO datacontrol(.Clk(Clk), 
						  .Reset(Reset_h),
						  .SRAM_OE_N(SRAM_OE_N), 
						  .SRAM_WE_N(SRAM_WE_N),
						  .parseddata(data), 
						  .SRAMREADDATA(SRAMREADDATA),
						  .GAMEDATA(GAMEDATA), 
						  .SRAMWRITEDATA(SRAMWRITEDATA)
					);								
/*******************************GLITCHFIX***********************************/
//hack to sync sram and onchip

glitchfix #(.N(16)) sramr(.Clk(Clk), 
						.Reset(Reset_h), 
						.Transfer(sramread),
						.Reg_In(GAMEDATA),
						.Reg_Out(framedata)
						);
						
logic [7:0] bf2out; //, buff2out;		
glitchfix #(.N(8)) srambf(.Clk(Clk), 
						.Reset(Reset_h), 
						.Transfer(read),
						.Reg_In(buffer2out),
						.Reg_Out(bf2out)
						);	
						
/***************************PIXEL MAPPER************************************/	
    color_mapper color_instance(	.Clk(Clk), 
											.Reset(Reset_h),
											.DrawX(drawxsig), 
											.DrawY(drawysig),
											.GAMEDATA(framedata),
											.buffer2out(bf2out),
											.switch(switch),
											.Red(VGA_R), 
											.Green(VGA_G), 
											.Blue(VGA_B),
											.drawaddr(drawaddr),
											.drawbuff2addr(drawbuff2addr),
											.sramread(sramread), 
											.read(read),
											.SRAM_OE_N(SRAM_OE_N)
										);														
										
/***********************KEYCODE HEXS****************************************/										
	 HexDriver hex_inst_0 (keycode[3:0], HEX0);
	 HexDriver hex_inst_1 (keycode[7:4], HEX1);
	 HexDriver hex_inst_2 (keycode[11:8], HEX2);
	 HexDriver hex_inst_3 (keycode[15:12], HEX3);	 
endmodule
