module dataparser(input Clk, Reset, writesig, disappear, transparent, //vssig,//write to sram or not?
						input [7:0] FLASHDATA,
						input [6:0] sel, enemyselect,
						input [1:0] special, hitvalid, skyhitvalid,
						input [3:0] skysel, samsel,
						input [10:0] shape_x, DrawX, soldierx,//choose x and y to draw at a certain location
						input [10:0] shape_y, DrawY, soldiery,//probably from a sprite.sv helper
						output logic flread, switch, 
						output logic [2:0] enemyhit, playerhit,//check if one frame done. If 1 then busy, 0 then ready.
						output logic [22:0] ADDR,	//to flash	
					   output logic [15:0] parseddata,
						output logic [19:0] copyaddr,						
						output logic [7:0] buffer2data,
						output logic [18:0] buffer2addr
						);

/*
* Write to one memory and then to the other. Switches between the two double buffer						
*/					
	//logic spriteon, backgroundon; //draw sprite or not. May need more logic for multiple layers.
	parameter [10:0] shapexsize = 140;
	parameter [10:0] shapeysize = 124;
	parameter [10:0] solxsize = 45;
	parameter [10:0] solysize = 100;
	parameter [10:0] solspecxsize = 54;	
	parameter [10:0] soldeathxsize = 70;	
	parameter [10:0] shape_x_size = 640;
	parameter [10:0] shape_y_size = 480;
	
	parameter [10:0] samuraihealthx = 250;
	parameter [10:0] samuraihealthy = 50;
	parameter [10:0] skywalkerhealthx = 100; 
	parameter [10:0] skywalkerhealthy = 25;
	
	logic [10:0] disty; //absolute distance
	assign disty = (shape_y>soldiery)?(shape_y-soldiery):(soldiery-shape_y);
	
	logic [10:0] x,y; //counters	
	logic [2:0] layer;
	logic first;
	logic [2:0] layerdone; //which layer has been done?

/***********************************DATA SELECTION***************************************/	
    always_ff@(posedge Clk or posedge Reset)
    begin	
		  if(Reset)
		  begin
				x <= 0;
				y <= 0;
				layer <= 3'b000;
				flread <= 1'b0;
				first <= 1'b1;
				switch <= 1'b1;
				layerdone <= 4'b0000;
				enemyhit <= 3'b000;
				playerhit <= 3'b000;
		  end  
		  else if((layer == 3'b000)) //background left
		  begin			  
				if(writesig || first)
				begin
					flread <= 1'b1;
					first <= 1'b0;
					
					if((shape_x) >= 10) //scrolling
					begin
						if(x + shape_x - 10 < 640)
							ADDR <= 640*y + x + shape_x - 10;	
						else
							ADDR <= 640*y + x + shape_x - 10 - 640 + 307200;							
					end
					else
						ADDR <= 640*y + x;					

						case(switch) //which buffer to write into?
							1'b1: buffer2addr <= 640*y + x;
							1'b0: copyaddr <= 640*y + x;
						endcase
						
						if(x==639)
						begin
							x <= 11'b0;				
							if(y==480)
							begin
								first <= 1'b1;							
								if((shape_y+24) < soldiery)
								begin
									layer <= 3'b010;
									y <= shape_y;
									x <= shape_x;								
								end
								else
								begin
									layer <= 3'b001;
									y <= soldiery;
									x <= soldierx;
								end
							end
							else
								y <= y+1;
						end	
						else
							x <= x+1;
				end
				else
				begin
					flread <= 1'b0;
					x <= x;
					y <= y;
					ADDR <= ADDR;
					copyaddr <= copyaddr;
					buffer2addr <= buffer2addr;
				end
		  end
//	  
		  else if((layer == 3'b001)) //skywalker
			begin	
				if(writesig || first)
				begin
					flread <= 1'b1;
					first <= 1'b0;
					
//						if(skyhitvalid == 2'b01 && //LCOLLISION
//							((soldierx - shape_x) <= 35 &&
//							(soldierx - shape_x) >= 0) &&
//							disty <= 20 
//							)
//						begin
//							playerhit <= 3'b001;
//						end
//						else if(skyhitvalid == 2'b10 && //RCOLLISION
//								(soldierx - shape_x) <= 75 &&
//								(soldierx - shape_x) >= 0 &&
//								disty <= 20 
//								)
//						begin
//							playerhit <= 3'b001;
//						end
//						else
//						begin
//							playerhit <= 3'b000;
//						end
	
							if (((x >= soldierx) && 
						  (x < (soldierx+solxsize)) &&
						  (y >= soldiery) && 
						  (y < (soldiery+solysize))) && (special == 2'b00))
						  begin								
							case(enemyselect)							
								//	6'd1: ADDR <= (270*(y-soldiery) + (x-soldierx))+416640+614400; //LREST/WAIT
									6'd6: ADDR <= (270*(y-soldiery) + (x-soldierx+225))+416640+614400; //LMOVE6
									6'd5: ADDR <= (270*(y-soldiery) + (x-soldierx+180))+416640+614400; //LMOVE5
									6'd4: ADDR <= (270*(y-soldiery) + (x-soldierx+135))+416640+614400; //LMOVE4
									6'd3: ADDR <= (270*(y-soldiery) + (x-soldierx+90))+416640+614400; //LMOVE3
									6'd2: ADDR <= (270*(y-soldiery) + (x-soldierx+45))+416640+614400; //LMOVE2
									6'd1: ADDR <= (270*(y-soldiery) + (x-soldierx))+416640+614400; //LMOVE
									
								//	6'd12: ADDR <= (270*(y-soldiery) + (x-soldierx))+87750+416640+614400; //REST/WAIT
									6'd12: ADDR <= (270*(y-soldiery) + (x-soldierx+225))+87750+416640+614400; //RMOVE
									6'd11: ADDR <= (270*(y-soldiery) + (x-soldierx+180))+87750+416640+614400; //RMOVE2
									6'd10: ADDR <= (270*(y-soldiery) + (x-soldierx+135))+87750+416640+614400; //RMOVE3
									6'd9: ADDR <= (270*(y-soldiery) + (x-soldierx+90))+87750+416640+614400; //RMOVE4
									6'd8: ADDR <= (270*(y-soldiery) + (x-soldierx+45))+87750+416640+614400; //RMOVE5
									6'd7: ADDR <= (270*(y-soldiery) + (x-soldierx))+87750+416640+614400; //RMOVE6
								
									6'd13: ADDR <= (270*(y-soldiery+100) + (x-soldierx))+416640+614400; //LATTACK1						
									6'd18: ADDR <= (270*(y-soldiery+100) + (x-soldierx+125))+87750+416640+614400; //RATTACK1					
								//	6'd14: ADDR <= (270*(y-soldiery+100) + (x-soldierx+45))+416640+614400; //LATTACK2	
								//	6'd17: ADDR <= (270*(y-soldiery+100) + (x-soldierx+170))+87750+416640+614400; //RATTACK2						
									6'd15: ADDR <= (270*(y-soldiery+100) + (x-soldierx+100))+416640+614400; //LATTACK3					
									6'd16: ADDR <= (270*(y-soldiery+100) + (x-soldierx+225))+87750+416640+614400; //RATTACK3
	
									6'd19: ADDR <= (270*(y-soldiery+200) + (x-soldierx))+416640+614400; //Ldeath1
									6'd20: ADDR <= (270*(y-soldiery+200) + (x-soldierx+45))+416640+614400; //Ldeath2	
									
									6'd22: ADDR <= (270*(y-soldiery+200) + (x-soldierx+225))+87750+416640+614400; //Rdeath1
									6'd23: ADDR <= (270*(y-soldiery+200) + (x-soldierx+180))+87750+416640+614400; //Rdeath2									
									default: ADDR <= 0;
								endcase	
								
								if(disappear)
								begin
										buffer2addr <= 'h61A80; //gone
										copyaddr <= 'hfffff;
								end
								else
								begin
									case(switch)
									1'b1:
									begin
										case(FLASHDATA)
										'h4E: buffer2addr <= 'h61A80; //random location off screen
										default: buffer2addr <= 640*y + x;
										endcase
									end
									1'b0:
									begin
										case(FLASHDATA)
										'h4E: copyaddr <= 'hfffff;
										default: copyaddr <= 640*y + x;
										endcase
									end
									endcase
								end
								
							end
							else if (((x >= soldierx) && 
						  (x < (soldierx+solspecxsize)) &&
						  (y >= soldiery) && 
						  (y < (soldiery+solysize))) && (special == 2'b01))
						  begin
								 case(enemyselect)
									6'd14: ADDR <= (270*(y-soldiery+100) + (x-soldierx+45))+416640+614400; //LATTACK2	
									6'd17: ADDR <= (270*(y-soldiery+100) + (x-soldierx+170))+87750+416640+614400; //RATTACK2								
									default ;
								 endcase		
			
								if(disappear)
								begin
										buffer2addr <= 'h61A80; //gone
										copyaddr <= 'hfffff;
								end
								else
								begin
									case(switch)
									1'b1:
									begin
										case(FLASHDATA)
										'h4E: buffer2addr <= 'h61A80; //random location off screen
										default: buffer2addr <= 640*y + x;
										endcase
									end
									1'b0:
									begin
										case(FLASHDATA)
										'h4E: copyaddr <= 'hfffff;
										default: copyaddr <= 640*y + x;
										endcase
									end
									endcase
								end	
						  end
							else if (((x >= soldierx) && 
						  (x < (soldierx+soldeathxsize)) &&
						  (y >= soldiery) && 
						  (y < (soldiery+solysize))) && (special == 2'b10))
						  begin	
								 case(enemyselect)
									6'd21: ADDR <= (270*(y-soldiery+200) + (x-soldierx+90))+416640+614400; //Ldeath3
									6'd24: ADDR <= (270*(y-soldiery+200) + (x-soldierx+110))+87750+416640+614400; //Rdeath3								
									default ;
								 endcase		
			
								if(disappear)
								begin
										buffer2addr <= 'h61A80; //gone
										copyaddr <= 'hfffff;
								end
								else
								begin
									case(switch)
									1'b1:
									begin
										case(FLASHDATA)
										'h4E: buffer2addr <= 'h61A80; //random location off screen
										default: buffer2addr <= 640*y + x;
										endcase
									end
									1'b0:
									begin
										case(FLASHDATA)
										'h4E: copyaddr <= 'hfffff;
										default: copyaddr <= 640*y + x;
										endcase
									end
									endcase
								end	
							end
							if(special == 2'b01)
							begin
								if(x==soldierx+solspecxsize)
								begin
									x <= soldierx;				
									if(y==soldiery+solysize)
									begin
										first <= 1'b1;
										layerdone[0] <= 1'b1;
										if(layerdone[1] == 1'b1)
										begin
											layer <= 3'b011; //health
											x <= 0;
											y <= 0;
										end
										else
										begin
											layer <= 3'b010;	
											y <= shape_y;
											x <= shape_x;										
										end
									end
									else
										y <= y+1;
								end	
								else
									x <= x+1;	
							end
							else if(special == 2'b00)
							begin
								if(x==soldierx+solxsize)
								begin
									x <= soldierx;				
									if(y==soldiery+solysize)
									begin
										first <= 1'b1;
										layerdone[0] <= 1'b1;
										if(layerdone[1] == 1'b1)
										begin
											layer <= 3'b011; //health
											x <= 0;
											y <= 0;
										end
										else
										begin
											layer <= 3'b010;	
											y <= shape_y;
											x <= shape_x;										
										end
									end
									else
										y <= y+1;
								end	
								else
									x <= x+1;								
							end
							else if(special == 2'b10)
							begin
								if(x==soldierx+soldeathxsize)
								begin
									x <= soldierx;				
									if(y==soldiery+solysize)
									begin
										first <= 1'b1;
										layerdone[0] <= 1'b1;
										if(layerdone[1] == 1'b1)
										begin
											layer <= 3'b011; //health
											x <= 0;
											y <= 0;
										end
										else
										begin
											layer <= 3'b010;	
											y <= shape_y;
											x <= shape_x;										
										end
									end
									else
										y <= y+1;
								end	
								else
									x <= x+1;								
							end							
				end
				else
				begin
					flread <= 1'b0;
					x <= x;
					y <= y;
					ADDR <= ADDR;
					copyaddr <= copyaddr;
					buffer2addr <= buffer2addr;
				end
			end
//	
		  else if((layer == 3'b010))//samurai
		  begin
			if(writesig || first)
			begin		  
					flread <= 1'b1;
					first <= 1'b0;	
					
//					if(transparent)
//					begin
//					 enemyhit <= 0;
//					end
//					else
//					begin
						if(hitvalid == 2'b01 && //LCOLLISION
							(((soldierx - shape_x) <= 25 &&
							(soldierx - shape_x) >= 0) ||
							((shape_x - soldierx) <= 40 &&
							(shape_x - soldierx) >= 0)) &&
							disty <= 20 
							)
						begin
							enemyhit <= 3'b001;
						end
						else if(hitvalid == 2'b10 && //RCOLLISION
								(soldierx - shape_x) <= 135 &&
								(soldierx - shape_x) >= 70 &&
								disty <= 20
								)
						begin
							enemyhit <= 3'b001;
						end	
						else
						begin
							enemyhit <= 3'b000;
						end
			//		end
					
					if ((x>=shape_x) && 
					(x<(shape_x+shapexsize)) && 
					(y>=shape_y) && 
					(y < (shape_y+shapeysize)))
					begin						
					case(sel)							
							6'b000101: ADDR <= (840*(y-shape_y) + (x-shape_x+140))+614400; //LREST/WAIT
							6'b001100: ADDR <= (840*(y-shape_y) + (x-shape_x))+614400; //LMOVE6
							6'b001101: ADDR <= (840*(y-shape_y) + (x-shape_x+140))+614400; //LMOVE5
							6'b001110: ADDR <= (840*(y-shape_y) + (x-shape_x+280))+614400; //LMOVE4
							6'b001111: ADDR <= (840*(y-shape_y) + (x-shape_x+420))+614400; //LMOVE3
							6'b010000: ADDR <= (840*(y-shape_y) + (x-shape_x+560))+614400; //LMOVE2
							6'b010001: ADDR <= (840*(y-shape_y) + (x-shape_x+700))+614400; //LMOVE
							
							6'b000110: ADDR <= (840*(y-shape_y) + (x-shape_x+560))+208320+614400; //REST/WAIT
							6'b010010: ADDR <= (840*(y-shape_y) + (x-shape_x))+208320+614400; //RMOVE
							6'b010011: ADDR <= (840*(y-shape_y) + (x-shape_x+140))+208320+614400; //RMOVE2
							6'b010100: ADDR <= (840*(y-shape_y) + (x-shape_x+280))+208320+614400; //RMOVE3
							6'b010101: ADDR <= (840*(y-shape_y) + (x-shape_x+420))+208320+614400; //RMOVE4
							6'b010110: ADDR <= (840*(y-shape_y) + (x-shape_x+560))+208320+614400; //RMOVE5
							6'b010111: ADDR <= (840*(y-shape_y) + (x-shape_x+700))+208320+614400; //RMOVE6
						
							6'b000011: ADDR <= (840*(y-shape_y+123) + (x-shape_x+420))+614400; //LATTACK1						
							6'b001000: ADDR <= (840*(y-shape_y+123) + (x-shape_x+280))+208320+614400; //RATTACK1					
							6'b000010: ADDR <= (840*(y-shape_y+123) + (x-shape_x+280))+614400; //LATTACK2	
							6'b001001: ADDR <= (840*(y-shape_y+123) + (x-shape_x+420))+208320+614400; //RATTACK2						
							6'b000000: ADDR <= (840*(y-shape_y+123) + (x-shape_x+140))+614400; //LATTACK3
							6'b000001: ADDR <= (840*(y-shape_y+123) + (x-shape_x))+614400; //LATTACK4						
							6'b001010: ADDR <= (840*(y-shape_y+123) + (x-shape_x+560))+208320+614400; //RATTACK3
							6'b001011: ADDR <= (840*(y-shape_y+123) + (x-shape_x+700))+208320+614400; //RATTACK4

							6'b000100: ADDR <= (840*(y-shape_y+123) + (x-shape_x+560))+614400; //LJUMP
							6'b000111: ADDR <= (840*(y-shape_y+123) + (x-shape_x+140))+208320+614400; //RJUMP

							6'd24: ADDR <= (840*(y-shape_y+123) + (x-shape_x+700))+614400; //LDeath
							6'd25: ADDR <= (840*(y-shape_y+123) + (x-shape_x))+208320+614400; //RDeath								
							default: ADDR <= 0;
						endcase	
						
								case(switch)
								1'b1:
								begin
									case(FLASHDATA)
									'h0E: buffer2addr <= 'h61A80;
									default: buffer2addr <= 640*y + x;
									endcase
								end
								1'b0:
								begin
									case(FLASHDATA)
									'h0E: copyaddr <= 'hfffff;
									default: copyaddr <= 640*y + x;
									endcase
								end
								endcase
								
						end
						if(x==shape_x+shapexsize)
						begin
							x <= shape_x;				
							if(y==(shape_y+shapeysize))
							begin
								first <= 1'b1;
								layerdone[1] <= 1'b1;
								if(layerdone[0] == 1'b1)
								begin
									layer <= 3'b011; //health
									x <= 11'b0;
									y <= 11'b0;
								end
								else
								begin
									layer <= 3'b001;
									y <= soldiery;
									x <= soldierx;
								end
							end
							else
								y <= y+1;
						end	
						else
							x <= x+1;	
				end
				else
				begin
					flread <= 1'b0;
					x <= x;
					y <= y;
					ADDR <= ADDR;
					copyaddr <= copyaddr;
					buffer2addr <= buffer2addr;
				end
			end		
		  else if(layer == 3'b100) //health
		  begin
			if(writesig || first)
			begin		  
					flread <= 1'b1;
					first <= 1'b0;	

								if ((x>=10) && 
								(x<(10 + samuraihealthx)) && 
								(y>=0) && 
								(y < (samuraihealthy)))
								begin						
									case(samsel)							
										4'd0: ADDR <=  (250*y + (x-10))+614400+416640+175500;
										4'd1: ADDR <=  (250*(y+50) + (x-10))+614400+416640+175500;
										4'd2: ADDR <=  (250*(y+100) + (x-10))+614400+416640+175500;
										4'd3: ADDR <=  (250*(y+150) + (x-10))+614400+416640+175500;
										4'd4: ADDR <=  (250*(y+200) + (x-10))+614400+416640+175500;
										4'd5: ADDR <=  (250*(y+250) + (x-10))+614400+416640+175500;
										4'd6: ADDR <=  (250*(y+300) + (x-10))+614400+416640+175500;
										default: ADDR <= 0;
									endcase	
											
								end									
						
						case(switch)
						1'b1:
						begin
							case(FLASHDATA)
							'h4E: buffer2addr <= 'h61A80;
							default: buffer2addr <= 640*y + x;
							endcase
						end
						1'b0:
						begin
							case(FLASHDATA)
							'h4E: copyaddr <= 'hfffff;
							default: copyaddr <= 640*y + x;
							endcase
						end
						endcase						
					
						if(x==10 + samuraihealthx)
						begin
							x <= 10;				
							if(y==50)
							begin
								layer <= 3'b101; //last one for sure
								first <= 1'b1;
								layerdone[2] <= 1'b1;
							end
							else
								y <= y+1;
						end	
						else
							x <= x+1;	
				end
				else
				begin
					flread <= 1'b0;
					x <= x;
					y <= y;
					ADDR <= ADDR;
					copyaddr <= copyaddr;
					buffer2addr <= buffer2addr;
				end		  
		  end
		  else if(layer == 3'b101) //health
		  begin
			if(writesig || first)
			begin		  
					flread <= 1'b1;
					first <= 1'b0;		
								
								if ((x>=390) && 
								(x<(390 + skywalkerhealthx)) && 
								(y>=0) && 
								(y < (skywalkerhealthy)))
								begin						
									case(skysel)							
										4'd0: ADDR <=  (100*y + (x-390))+87500+614400+416640+175500;
										4'd1: ADDR <=  (100*(y+25) + (x-390))+87500+614400+416640+175500;
										4'd2: ADDR <=  (100*(y+50) + (x-390))+87500+614400+416640+175500;
										4'd3: ADDR <=  (100*(y+75) + (x-390))+87500+614400+416640+175500;
										default: ADDR <= 0;
									endcase	
											
								end								
						
						case(switch)
						1'b1:
						begin
							case(FLASHDATA)
							'h4E: buffer2addr <= 'h61A80;
							default: buffer2addr <= 640*y + x;
							endcase
						end
						1'b0:
						begin
							case(FLASHDATA)
							'h4E: copyaddr <= 'hfffff;
							default: copyaddr <= 640*y + x;
							endcase
						end
						endcase						
					
						if(x==639)
						begin
							x <= 390;				
							if(y==25)
							begin
								layer <= 3'b011; //last one for sure
								first <= 1'b1;
								layerdone[2] <= 1'b1;
							end
							else
								y <= y+1;
						end	
						else
							x <= x+1;	
				end
				else
				begin
					flread <= 1'b0;
					x <= x;
					y <= y;
					ADDR <= ADDR;
					copyaddr <= copyaddr;
					buffer2addr <= buffer2addr;
				end		  
		  end			
			else if(layer == 3'b011)
			begin
				if((DrawX==799) && (DrawY==524))
				begin
					layer <= 3'b000; //hold until signal changes
					flread <= 1'b0;
					layerdone <= 3'b000;
					first <= 1'b1;
					x<=0;
					y<=0;
					switch <= ~switch; //switch buffers
				end
				else
				begin
					layer <= 3'b011;
					flread <= 1'b0;
				end
			end		
     end
	 
	logic transparency;
	logic [2:0] collision; //should be output of state machine
	/* collision:
	*	environmental collision: 011
	*  kill collision: 001
	*	hurt collision: 010
	*	no collision: 000
	*/

/******************************************DATA PARSING***********************************/	
	  assign buffer2data = FLASHDATA;

	  always_comb
	  begin
			transparency = 0;
			collision = 3'b000; //ADD COLLISION IN EACH IF ELSE STATEMENT
			
			if((FLASHDATA != 'h0E) && (layer==3'b010)) //samurai
			begin
				transparency = 0;
				parseddata = {FLASHDATA, transparency, layer, collision, 1'b0};
			end
			else if((FLASHDATA == 'h0E) && (layer==3'b010)) //bounding box
			begin
				transparency = 1;
				parseddata = {FLASHDATA, transparency, layer, collision, 1'b0};			
			end		
			else if((FLASHDATA != 'h4E) && (layer == 3'b001)) //soldier
			begin
				transparency = 0;
				parseddata = {FLASHDATA, transparency, layer, collision, 1'b0};			
			end
			else if((FLASHDATA == 'h4E) && (layer == 3'b001)) //bounding box
			begin
				transparency = 1;
				parseddata = {FLASHDATA, transparency, layer, collision, 1'b0};			
			end
			else if(layer == 3'b000) //background
			begin
				transparency = 0;
				parseddata = {FLASHDATA, transparency, layer, collision, 1'b0}; //make sure collision is 000
			end			
			else
			begin
				parseddata = 16'hffff;
			end
	  end	 
endmodule