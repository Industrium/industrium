module sramtristate#(N = 20) (
	input [N-1:0] copyaddr,
					drawaddr,
	input copy, draw,
	output logic [N-1:0] SRAM_ADDR
);

always_comb
begin
	if(copy && !draw)
		SRAM_ADDR = copyaddr;
	else if(!copy && draw)
		SRAM_ADDR = drawaddr;
	else
		SRAM_ADDR = 0;
end

endmodule
