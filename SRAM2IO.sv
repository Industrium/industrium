module  SRAM2IO (input Clk, Reset,
					input SRAM_OE_N, SRAM_WE_N,
					input [15:0] parseddata, SRAMREADDATA,
					output logic [15:0] GAMEDATA, SRAMWRITEDATA
					);	 
	
	always_comb
    begin
        GAMEDATA = 16'd0;
        if (SRAM_WE_N  && ~SRAM_OE_N) //active low
				GAMEDATA = SRAMREADDATA;
    end	 
	
assign SRAMWRITEDATA = (~SRAM_WE_N) ? parseddata : 16'bZZZZZZZZZZZZZZZZ;
endmodule
