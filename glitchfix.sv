module glitchfix #(N = 16)(input Clk, Reset, Transfer,
              input  [N-1:0]  Reg_In,
              output logic [N-1:0]  Reg_Out);

    always_ff @ (negedge Clk or posedge Reset)
    begin
 
    if (Reset) 
        Reg_Out <= 8'h00;
	 else if (Transfer)
        Reg_Out <= Reg_In;	
    end

endmodule
