/************************************************************************/
/*----------------------------SKYWALKER HEALTH-------------------------*/
/************************************************************************/
module skyhealth(input Clk, Reset, //I assume the clock is the vertical sync? So that the next sprite is drawn?
						input [2:0] enemyhit,			
						output logic [3:0] skysel, //how the sprite will move
						output logic enemydeath
						);
						
enum logic[1:0] {SKYHEALTH1, SKYHEALTH2, SKYHEALTH3, 
					 SKYFULLHEALTH
					} state, nextstate;
	
logic [1:0] counter;
									
always_ff@(posedge Clk or posedge Reset)
	begin 
		if(Reset)
		begin
			state <= SKYFULLHEALTH; //default to looking right
			counter <= 0;
		end
		else if (counter == 2'b11)
			begin
				state <= nextstate;
				counter <= 0;
			end
		else
			counter <= counter + 1;			
	end

		//Assign outputs based on "state"
always_comb
	begin
		nextstate = state; //Guarantees that it is replaced

/*******************Define state transition***************************/

		case(state)
			SKYFULLHEALTH:
			begin
				if(enemyhit == 3'b001)
					nextstate = SKYHEALTH1;
				else
					nextstate = SKYFULLHEALTH;
			end
			SKYHEALTH1: 
			begin
				if(enemyhit == 3'b001)
					nextstate = SKYHEALTH2;
				else
					nextstate = SKYHEALTH1;
			end			
			SKYHEALTH2:
			begin
				if(enemyhit == 3'b001)
					nextstate = SKYHEALTH3;
				else
					nextstate = SKYHEALTH2;
			end				
			SKYHEALTH3:
			begin
					nextstate = SKYFULLHEALTH;
			end		
		endcase


		enemydeath = 1'b0;
/*************************Define signals******************************/		
		case(state)
			SKYFULLHEALTH:	skysel = 4'd0;	
			SKYHEALTH1: skysel = 4'd1;
			SKYHEALTH2: skysel = 4'd2;
			SKYHEALTH3:
			begin
				skysel = 4'd3;
				enemydeath = 1'b1;
			end
		endcase
	end
endmodule	