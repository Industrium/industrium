module buffer 
#(parameter N=16)
(
	input Clk, Reset,
	input enable,
	input [N-1:0] sr_in,
	output logic [N-9:0] sr_out
);

	// Declare the shift register
	reg [N-1:0] sr;
	logic counter;
	logic clkdiv;
	
    always_ff @ (posedge Clk or posedge Reset ) //if we're matching 50MHz clock, we do 
    begin 													//clkdiv for a 2-bit counter to buffer only twice
        if (Reset) 
            clkdiv <= 1'b0;
        else 
            clkdiv <= ~ (clkdiv);
    end	
	
	// Shift everything over, load the incoming bit
	always_ff@(posedge clkdiv or posedge Reset)
	begin
		if(Reset)
		begin
			counter <= 1'b1;
			sr <= 0;
		end
		else if(enable ==1'b1)
		begin
			sr[N-1:N-8] <= sr[N-9:0];
			if(counter==1'b1)
			begin
				sr <= sr_in;
				counter <= 0;
			end
			else
			begin
				counter <= counter + 1;
			end
		end
	end

	// Catch the outgoing bits
	always_comb
	begin
		sr_out = sr[N-1:N-8];
	end

endmodule