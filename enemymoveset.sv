module enemymoveset(input Clk, Reset, //I assume the clock is the vertical sync? So that the next sprite is drawn?
						input enemydeath,
						input [10:0] soldierxmotion, soldierymotion, //enemy motions
						input [10:0] soldierxpos, soldierypos,	
						input [10:0] spritexpos, spriteypos,						
						output logic [6:0] motion, //how the sprite will move
						output logic [1:0] special, skyhitvalid,//special sizes: 1=middle attack, 2=death
						output logic disappear, transparency
						);
						
/* This module is for giving the enemy movement.
* Theoretically input collision so it chooses death state
*/

enum logic[4:0] {LEFTMOVE, LEFTMOVE2, LEFTMOVE3, LEFTMOVE4, LEFTMOVE5, LEFTMOVE6,
					RIGHTMOVE, RIGHTMOVE2, RIGHTMOVE3, RIGHTMOVE4, RIGHTMOVE5, RIGHTMOVE6,
					LATTACK1, LATTACK2, LATTACK3,
					RATTACK1, RATTACK2, RATTACK3, 
					LDEATH, LDEATH2, LDEATH3, 
					RDEATH, RDEATH2, RDEATH3,
					VANISH,	//Death
					RWAIT, LWAIT} curr_state, next_state;
									
/* LWAIT and RWAIT are used to keep track of the sprite direction. For instance when 
* we're going right, and we're not pressing the right arrow, then we want
* the sprite to face right. Vice versa for left
*/

logic [2:0] counter; //counter to slow draw so that character moves fluidly
logic [2:0] count; //counter specifically for the attack of enemy
logic [10:0] distx, disty; //relative distance between sprites

always_ff@(posedge Clk or posedge Reset)
	begin 
		if(Reset)
		begin
			curr_state <= LWAIT; //default to looking right
			counter <= 0;
			count <= 0;
		end
		else if(counter == 3'b101)
			begin
				curr_state <= next_state;
				counter <= 0;
			end
		else if(curr_state == LATTACK3 || 
					curr_state == RATTACK3 ||
					curr_state == LDEATH ||
					curr_state == LDEATH2 ||
					curr_state == LDEATH3 ||
					curr_state == RDEATH ||
					curr_state == RDEATH2 ||
					curr_state == RDEATH3 ||					
					curr_state == VANISH)
			begin
				count <= count+1;
				counter <= counter + 1;
				if(count == 3'b111)
					count <= 0;
			end
		else
			counter <= counter + 1;
	end

		//Assign outputs based on "state"
always_comb
	begin
		next_state = curr_state; //Guarantees that it is replaced
		distx = (spritexpos>soldierxpos)?(spritexpos-soldierxpos):(soldierxpos-spritexpos); //absolute values
		disty = (spriteypos>soldierypos)?(spriteypos-soldierypos):(soldierypos-spriteypos);

/*******************Define state transition***************************/

		case(curr_state)
			RWAIT: 
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE;					
					else //equals
						next_state = RWAIT;
				end
				
			LWAIT: 
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;				
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE;							
					else //equals
						next_state = LWAIT;								
				end						

/*************************RIGHT************************************/

			RIGHTMOVE:
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;				
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE2;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE2;					
					else //equals
						next_state = RWAIT;
				end

			RIGHTMOVE2: 
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;				
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE3;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE2;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE3;					
					else //equals
						next_state = RWAIT;
				end
							  
			RIGHTMOVE3:						
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;				
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE4;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE3;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE4;					
					else //equals
						next_state = RWAIT;
				end

			RIGHTMOVE4:						
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;				
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE5;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE4;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE5;					
					else //equals
						next_state = RWAIT;
				end

			RIGHTMOVE5:						
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;				
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE6;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE5;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE6;					
					else //equals
						next_state = RWAIT;
				end				

			RIGHTMOVE6:						
				begin
					if(enemydeath == 1'b1)
						next_state = RDEATH;				
					else if((distx <= 80) && (disty <= 20)) //standard offsets
						next_state = RATTACK1;
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE6;
					else if (soldierymotion != 0)
						next_state = RIGHTMOVE;					
					else //equals
						next_state = RWAIT;
				end					
/**************************LEFT*************************/					

			LEFTMOVE:
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;				
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE2;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE2;							
					else //equals
						next_state = LWAIT;		
				end

			LEFTMOVE2: 
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;					
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE3;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE2;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE3;							
					else //equals
						next_state = LWAIT;	
				end
							  
			LEFTMOVE3:						
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;					
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE4;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE3;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE4;							
					else //equals
						next_state = LWAIT;	
				end	
	
			LEFTMOVE4:						
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;					
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE5;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE4;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE5;							
					else //equals
						next_state = LWAIT;	
				end
	
			LEFTMOVE5:						
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;					
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE6;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE5;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE6;							
					else //equals
						next_state = LWAIT;	
				end		

			LEFTMOVE6:						
				begin
					if(enemydeath == 1'b1)
						next_state = LDEATH;					
					else if((distx <= 80) && (disty <= 20))
						next_state = LATTACK1;
					else if (soldierxmotion != 1)
						next_state = LEFTMOVE;				
					else if(soldierxmotion == 1)
						next_state = RIGHTMOVE6;	
					else if (soldierymotion != 0)
						next_state = LEFTMOVE;							
					else //equals
						next_state = LWAIT;		
				end	
				
/**************************LATTACK*************************/					

			LATTACK1: 
			begin
				if(enemydeath == 1'b1)
					next_state = LDEATH;	
				else
					next_state = LATTACK2; //to draw the attack correctly
			end
			LATTACK2: 
			begin
				if(enemydeath == 1'b1)
					next_state = LDEATH;	
				else
					next_state = LATTACK3; //to draw the attack correctly
			end
			LATTACK3: 
			begin
				if(enemydeath == 1'b1)
					next_state = LDEATH;				
				else if(((distx <= 70) && (disty <= 20)) && count == 3'b111)
					next_state = LATTACK1;
				else if(!((distx <= 70) && (disty <= 20)))
					next_state = LWAIT;						
				else
					next_state = LATTACK3;
	
			end
/**************************RATTACK*************************/	

			RATTACK1: 
			begin
				if(enemydeath == 1'b1)
					next_state = RDEATH;	
				else
					next_state = RATTACK2; //to draw the attack correctly
			end
			RATTACK2: 
			begin
				if(enemydeath == 1'b1)
					next_state = RDEATH;	
				else
					next_state = RATTACK3; //to draw the attack correctly
			end
			RATTACK3:
			begin
				if(enemydeath == 1'b1)
					next_state = RDEATH;				
				else if(((distx <= 70) && (disty <= 20)) && count == 3'b111)
					next_state = RATTACK1;
				else if(!((distx <= 70) && (disty <= 20)))
					next_state = RWAIT;
				else
					next_state = RATTACK3;		
			end			
			
/*************************DEATH************************************/

			LDEATH:
			begin
				if(count == 3'b111)
					next_state = LDEATH2;
				else
					next_state = LDEATH;					
			end
			LDEATH2:
			begin
				if(count == 3'b111)
					next_state = LDEATH3;
				else
					next_state = LDEATH2;					
			end
			LDEATH3:
			begin
				if(count == 3'b111)
					next_state = VANISH;
				else
					next_state = LDEATH3;				
			end
			RDEATH:
			begin
				if(count == 3'b111)
					next_state = RDEATH2;
				else
					next_state = RDEATH;					
			end
			RDEATH2:
			begin
				if(count == 3'b111)
					next_state = RDEATH3;
				else
					next_state = RDEATH2;					
			end
			RDEATH3:
			begin
				if(count == 3'b111)
					next_state = VANISH;
				else
					next_state = RDEATH3;				
			end			
			VANISH:
			begin
				if(count == 3'b111)
					next_state = LWAIT;
				else
					next_state = VANISH;				
			end
			
			default: ;
		endcase


		special = 2'b00; //default
		skyhitvalid = 2'b00;
		disappear = 1'b0;
				transparency = 1'b0;		
/*************************Define signals******************************/		
		case(curr_state)
			RWAIT: motion = 6'd12; //facing right rest position (SAME AS WALKING)	
			LWAIT: motion = 6'd1; //facing left rest position
			
/*************************RIGHT************************************/		
			RIGHTMOVE: motion = 6'd12; //18
			RIGHTMOVE2: motion = 6'd11; //19	  
			RIGHTMOVE3: motion = 6'd10;	//20	
			RIGHTMOVE4: motion = 6'd9;	//21
			RIGHTMOVE5: motion = 6'd8;	//22
			RIGHTMOVE6: motion = 6'd7;	//23	

/*****************LEFT*************************/							
			LEFTMOVE6: motion = 6'd6; //12
			LEFTMOVE5: motion = 6'd5; //13	
			LEFTMOVE4: motion = 6'd4; //14				
			LEFTMOVE3: motion = 6'd3; //15	
			LEFTMOVE2: motion = 6'd2; //16	
			LEFTMOVE:  motion = 6'd1; //17
			
/*****************LATTACK**********************/
			LATTACK1: motion = 6'd13; //0
			LATTACK2: 
			begin
				motion = 6'd14; //1
				special = 2'b01;
				skyhitvalid = 2'b01;
			end
			LATTACK3: motion = 6'd15; //2
/*****************RATTACK**********************/			
			RATTACK1: motion = 6'd16; //8
			RATTACK2: 
			begin
				motion = 6'd17; //9			
				special = 2'b01;
				skyhitvalid = 2'b10;
			end			
			RATTACK3: motion = 6'd18; //10
			
/*************************DEATH************************************/

			LDEATH: 
			begin
				motion = 6'd19;
				transparency = 1'b1;
			end
			LDEATH2:			
			begin
				motion = 6'd20;
				transparency = 1'b1;				
			end
			LDEATH3: 			
			begin
				special = 2'b10;
				motion = 6'd21;
				transparency = 1'b1;
			end
			RDEATH: 
			begin
				motion = 6'd22;
				transparency = 1'b1;
			end
			RDEATH2:			
			begin
				motion = 6'd23;
				transparency = 1'b1;
			end
			RDEATH3: 			
			begin
				special = 2'b10;
				motion = 6'd24;
				transparency = 1'b1;
			end			
			VANISH: 
			begin
				disappear = 1'b1;		
				motion = 6'b111111;
				transparency = 1'b1;
			end
		endcase
	end
	
endmodule