module memorycontroller ( 	input	Clk, 
											Reset,
											flread,
											switch,
								output logic FL_OE_N, transfer,
												 FL_CE_N,
												 SRAM_WE_N, writesig,
												 SRAM_CE_N			 
								);

    enum logic [3:0] { SRAMWRITE1, SRAMWRITE2,
							SRAMREAD1, SRAMREAD2, 
							FLASHOEFAST1,FLASHOEFAST2,FLASHOEFAST3,
							FLASHOE1, FLASHOE2, FLASHOE3, FLASHOE4, FLASHOE5,
							WAIT} state, next_state;   // Internal state logic
	    
logic [2:0] counter;
logic firstdone;	
	
	 always_ff @ (posedge Clk or posedge Reset )
    begin : Assign_Next_State
        if (Reset) 
		  begin
            state <= WAIT;
				counter <= 0;
				firstdone <= 0;
		  end
        else 
		  begin
					state <= next_state;
					
				  if(state == FLASHOEFAST3)
				  begin
						if(counter == 3'b111)
						begin
							firstdone <= 0;
						end
				  end
				  else if(state == FLASHOE5)
				  begin
						firstdone <= 1'b1;
				  end	
				  else if(state == SRAMWRITE2)
						counter <= counter + 1;
		  end
    end
   
	always_comb
    begin 
	    next_state  = state;

      unique case (state)
			SRAMWRITE1: next_state = SRAMWRITE2;
			SRAMWRITE2: next_state = WAIT;
//			FLASHOE1: next_state = FLASHOE2;
//			FLASHOE2: next_state = FLASHOE3;
//			FLASHOE3: next_state = FLASHOE4;
//			FLASHOE4: next_state = FLASHOE5;
//			FLASHOE5: next_state = WAIT; //changed
			FLASHOEFAST1: 
			begin
					next_state = FLASHOEFAST2;	
			end	
			FLASHOEFAST2:
			begin
				if(flread)
					next_state = FLASHOEFAST1;
				else
					next_state = WAIT;
			end
//			FLASHOEFAST3: next_state = WAIT; //changed
//			SRAMREAD1: 
//			begin
//				if(sramread)
//					next_state = SRAMREAD1;
//				else
//					next_state = WAIT;
//			end
//			SRAMREAD2: next_state = WAIT;
			WAIT:
			begin
				if(flread && firstdone)
					next_state = FLASHOEFAST1;
				else if(flread && !firstdone)
					next_state = FLASHOEFAST1;
//				else if(sramread)
//					next_state = SRAMREAD1;
				else
					next_state = WAIT;
			end
			default ;
		endcase
    end
   
    always_comb
    begin 
			SRAM_WE_N = 1'b1;
//			SRAM_OE_N = 1'b1;				
//			SRAM_CE_N = 1'b1;			
			FL_OE_N = 1'b1;
//			FL_CE_N = 1'b1;
			writesig = 1'b0;
//			readsig = 1'b0;
			transfer = 1'b0;
			
			unique case (state)
			SRAMWRITE1:
			begin
	//			SRAM_CE_N = 1'b0;				
				SRAM_WE_N = 1'b0;			
			end
			SRAMWRITE2:
			begin
	//			SRAM_CE_N = 1'b0;
				SRAM_WE_N = 1'b0;			
				writesig = 1'b1;			
			end
//			FLASHOE1: 
//			begin
////				FL_CE_N = 1'b0;			
//				FL_OE_N = 1'b0;
//			end
//			FLASHOE2: 
//			begin
////				FL_CE_N = 1'b0;
//				FL_OE_N = 1'b0;
//			end
//			FLASHOE3: 
//			begin
////				FL_CE_N = 1'b0;
//				FL_OE_N = 1'b0;
//			end
//			FLASHOE4: 
//			begin
////				FL_CE_N = 1'b0;
//				FL_OE_N = 1'b0;
//				SRAM_WE_N = 1'b0;
//			end
//			FLASHOE5:
//			begin
////				FL_CE_N = 1'b0;			
//				FL_OE_N = 1'b0;
//				transfer = 1'b1;
//				writesig = 1'b1;
//			end			
			FLASHOEFAST1: 
			begin
//				FL_CE_N = 1'b0;			
				FL_OE_N = 1'b0;
				if(switch == 1'b0)
				begin
					SRAM_WE_N = 1'b0;
				end		
			end
			FLASHOEFAST2: 
			begin
//				FL_CE_N = 1'b0;
				FL_OE_N = 1'b0;
				if(switch == 1'b0)
				begin
					SRAM_WE_N = 1'b0;
				end	
				writesig = 1'b1;
				transfer = 1'b1;				
			end
//			FLASHOEFAST3: 
//			begin
////				FL_CE_N = 1'b0;
//				FL_OE_N = 1'b0;
//				SRAM_WE_N = 1'b0;
//				writesig = 1'b1;
//				transfer = 1'b1;
//			end			
//			SRAMREAD1: 
//			begin
//	//			SRAM_CE_N = 1'b0;		
//				if(switch == 1'b1)
//					SRAM_OE_N = 1'b0;
//				readsig = 1'b1;
//			end
//			SRAMREAD2:
//			begin
//	//			SRAM_CE_N = 1'b0;			
//				SRAM_OE_N = 1'b0;
//				readsig = 1'b1;
//			end
			WAIT: ;
			default ;
         endcase
       end 
	
assign FL_CE_N = 1'b0;	
assign SRAM_CE_N = 1'b0;
endmodule