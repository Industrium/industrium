module healthstate(input Clk, Reset, //I assume the clock is the vertical sync? So that the next sprite is drawn?
						input [2:0] playerhit,					
						output logic [3:0] samsel, //how the sprite will move
						output logic playerdeath
						);
						
/* This module is for health.
* 
*/

enum logic[2:0] {SAMHEALTH1, SAMHEALTH2, SAMHEALTH3, SAMHEALTH4, SAMHEALTH5, SAMHEALTH6,
					 SAMFULLHEALTH
					} curr_state, next_state;
									
logic [1:0] counter;
									
always_ff@(posedge Clk or posedge Reset)
	begin 
		if(Reset)
		begin
			curr_state <= SAMFULLHEALTH; //default to looking right
			counter <= 0;
		end
		else if (counter == 2'b11)
			begin
				curr_state <= next_state;
				counter <= 0;
			end
		else
			counter <= counter + 1;	
	end

		//Assign outputs based on "state"
always_comb
	begin
		next_state = curr_state; //Guarantees that it is replaced

/*******************Define state transition***************************/

		case(curr_state)
			SAMFULLHEALTH:
			begin
				if(playerhit == 3'b001)
					next_state = SAMHEALTH1;
				else
					next_state = SAMFULLHEALTH;
			end
			SAMHEALTH1: 
			begin
				if(playerhit == 3'b001)
					next_state = SAMHEALTH2;
				else
					next_state = SAMHEALTH1;
			end			
			SAMHEALTH2:
			begin
				if(playerhit == 3'b001)
					next_state = SAMHEALTH3;
				else
					next_state = SAMHEALTH2;
			end				
			SAMHEALTH3:
			begin
				if(playerhit == 3'b001)
					next_state = SAMHEALTH4;
				else
					next_state = SAMHEALTH3;
			end			
			SAMHEALTH4: 
			begin
				if(playerhit == 3'b001)
					next_state = SAMHEALTH5;
				else
					next_state = SAMHEALTH4;
			end		
			SAMHEALTH5:
			begin
				if(playerhit == 3'b001)
					next_state = SAMHEALTH6;
				else
					next_state = SAMHEALTH5;
			end			
			SAMHEALTH6: //dead
			begin
//				if(count == 3'b111)
					next_state = SAMFULLHEALTH;
//				else
//					next_state = SAMHEALTH6;
			end					
		endcase


		playerdeath = 1'b0;
/*************************Define signals******************************/		
		case(curr_state)
			SAMFULLHEALTH:	samsel = 4'd0;	
			SAMHEALTH1: samsel = 4'd1;
			SAMHEALTH2: samsel = 4'd2;
			SAMHEALTH3: samsel = 4'd3;
			SAMHEALTH4: samsel = 4'd4;
			SAMHEALTH5: samsel = 4'd5;
			SAMHEALTH6:
			begin
				samsel = 4'd6;
				playerdeath = 1'b1;
			end
		endcase
	end
	
endmodule